
import { ClientService } from './services/client.service';
import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'financial-for-future';

  constructor(
    private readonly clientService: ClientService
  ) {}

  ngOnInit(): void {
    this.clientService.initializeClients();
  }

  ngOnDestroy(): void {
    this.clientService.clearClients();
  }
}
