export const clients = [
    {
        id: 1,
        firstName: 'Todor',
        middleName: 'Petrov',
        lastName: 'Ivanov',
        address: {
            city: 'Sofia',
            street: 'Vitosha 53',
            app: '1'
        },
        startDate: '20.1.2012',
        balance: 1000,
        cards: ['VISA', 'MASTER CARD'],
        loans: [{
            amount: 2000,
            startDate: '3.10.2018',
            endDate: '30.12.2020'
        }]
    },
    {
        id: 2,
        firstName: 'Petur',
        middleName: 'Ivanov',
        lastName: 'Todorov',
        address: {
            city: 'Sofia',
            street: 'Dojran 3',
            app: ''
        },
        startDate: '2.1.2011',
        balance: 2000,
        cards: [],
        loans: []
    },
    {
        id: 3,
        firstName: 'Sava',
        middleName: 'Zhelev',
        lastName: 'Zhelev',
        address: {
            city: 'Sofia',
            street: 'Mladost 1A, bl.31',
            app: '1'
        },
        startDate: '20.1.2012',
        balance: 1000,
        cards: ['VISA'],
        loans: []
    },
    {
        id: 4,
        firstName: 'Boyko',
        middleName: 'Methodiev',
        lastName: 'Borisov',
        address: {
            city: 'Bankya',
            street: 'Some street 53',
            app: ''
        },
        startDate: '20.1.2010',
        balance: 1000,
        cards: [],
        loans: [{
            amount: 500,
            startDate: '3.10.2019',
            endDate: '30.12.2020'
        }]
    },
    {
        id: 5,
        firstName: 'Rosen',
        middleName: 'Urkov',
        lastName: 'Ivanov',
        address: {
            city: 'Sofia',
            street: 'Vitosha 53',
            app: '1'
        },
        startDate: '20.1.2012',
        balance: 1000,
        cards: [],
        loans: []
    },
    {
        id: 6,
        firstName: 'Trifon',
        middleName: 'Georgiev',
        lastName: 'Dachev',
        address: {
            city: 'Sofia',
            street: 'Manastirski livadi, bl. 34',
            app: '1'
        },
        startDate: '20.1.2010',
        balance: 5000,
        cards: [],
        loans: []
    },
    {
        id: 7,
        firstName: 'Natalia',
        middleName: 'Petrova',
        lastName: 'Ivanova',
        address: {
            city: 'Sofia',
            street: 'Vitosha 3',
            app: '4'
        },
        startDate: '20.1.2013',
        balance: 5000,
        cards: ['MASTER CARD'],
        loans: []
    },
    {
        id: 8,
        firstName: 'Boris',
        middleName: 'Petrov',
        lastName: 'Dinev',
        address: {
            city: 'Sofia',
            street: 'Vitosha 5',
            app: '5'
        },
        startDate: '20.1.2013',
        balance: 1000,
        cards: ['VISA', 'MASTER CARD'],
        loans: []
    },
    {
        id: 9,
        firstName: 'Nevena',
        middleName: 'Petrova',
        lastName: 'Ivanova',
        address: {
            city: 'Sofia',
            street: 'Vitosha 13',
            app: '10'
        },
        startDate: '20.1.2013',
        balance: 1000,
        cards: ['VISA'],
        loans: [{
            amount: 5000,
            startDate: '3.10.2019',
            endDate: '30.12.2020'
        }]
    },
    {
        id: 10,
        firstName: 'Ralitsa',
        middleName: 'Petrova',
        lastName: 'Vasileva',
        address: {
            city: 'Sofia',
            street: 'Vitosha 13',
            app: '13'
        },
        startDate: '20.1.2014',
        balance: 1000,
        cards: ['VISA'],
        loans: []
    },
    {
        id: 11,
        firstName: 'Sonya',
        middleName: 'Petrova',
        lastName: 'Veleva',
        address: {
            city: 'Sofia',
            street: 'Vitosha 73',
            app: '131'
        },
        startDate: '20.1.2015',
        balance: 1000,
        cards: ['VISA'],
        loans: []
    },
    {
        id: 12,
        firstName: 'Radan',
        middleName: 'Petrov',
        lastName: 'Kanev',
        address: {
            city: 'Sofia',
            street: 'Vitosha 103',
            app: '16'
        },
        startDate: '20.1.2017',
        balance: 1050,
        cards: ['VISA'],
        loans: []
    },
    {
        id: 13,
        firstName: 'John',
        middleName: 'Jay',
        lastName: 'Travolta',
        address: {
            city: 'Sofia',
            street: 'Vitosha 122',
            app: '100'
        },
        startDate: '20.1.2016',
        balance: 1200,
        cards: ['VISA'],
        loans: []
    },
    {
        id: 14,
        firstName: 'Sergey',
        middleName: 'Petrov',
        lastName: 'Vasilev',
        address: {
            city: 'Sofia',
            street: 'Vitosha 103',
            app: '137'
        },
        startDate: '20.1.2013',
        balance: 4000,
        cards: ['VISA'],
        loans: []
    },
    {
        id: 15,
        firstName: 'Damian',
        middleName: 'Savov',
        lastName: 'Savov',
        address: {
            city: 'Sofia',
            street: 'Vitosha 133',
            app: '143'
        },
        startDate: '20.1.2013',
        balance: 1300,
        cards: ['VISA'],
        loans: []
    }
];

