import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Client } from '../../common/Client';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  public clientForm: FormGroup ;
  public clientLoanForm: FormGroup;

  @Input() client: Client;
  @Output() editedClient = new EventEmitter<Client>();

  constructor(private readonly fb: FormBuilder) { }

  ngOnInit(): void {
    this.clientForm = this.fb.group({
      firstName: [this.client.firstName, Validators.compose([Validators.required])],
      middleName: [this.client.middleName, Validators.compose([Validators.required])],
      lastName: [this.client.lastName, Validators.compose([Validators.required])],
      address: this.fb.group({
        city: [this.client.address.city, Validators.compose([Validators.required])],
        street: [this.client.address.street, Validators.compose([Validators.required])],
        app: [this.client.address.app ? this.client.address.app : ''],
      }),
      startDate: [this.client.startDate, Validators.compose([Validators.required])],
      balance: [this.client.balance, Validators.compose([Validators.required])],
      cards: [this.client.cards.toString().split(',').join(', ')]
    });

    this.clientLoanForm = this.fb.group({
      amount: ['', Validators.compose([Validators.required, Validators.max(10000), Validators.pattern('[0-9]*')])],
      startDate: ['', Validators.compose([Validators.required])],
      endDate: ['',  Validators.compose([Validators.required])]
    });

    if (this.client.loans.length > 0) {
      this.clientLoanForm.patchValue({
        amount: this.client.loans[0].amount,
        startDate: this.client.loans[0].startDate,
        endDate: this.client.loans[0].endDate
      });
    }
  }

  updateClient() {
    const loan = {
      amount: this.clientLoanForm.value.amount,
      startDate: this.clientLoanForm.value.startDate,
      endDate: this.clientLoanForm.value.endDate,
    };

    const newClient: Client = {
      id: this.client.id,
      firstName: this.clientForm.value.firstName,
      middleName: this.clientForm.value.middleName,
      lastName: this.clientForm.value.lastName,
      address: {
        city: this.clientForm.value.address.city,
        street: this.clientForm.value.address.street,
        app: this.clientForm.value.address.app,
      },
      startDate: this.clientForm.value.startDate,
      balance: this.clientForm.value.balance,
      cards: this.clientForm.value.cards.trim().length > 0 ?  this.clientForm.value.cards.trim().split(',') : [],
      loans: this.client.loans.length > 0 ? [loan] : []
    };

    this.editedClient.emit(newClient);
  }

  cancelUpdate() {
    this.editedClient.emit(this.client);
  }
}
