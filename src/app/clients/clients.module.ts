import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersComponent } from './users/users.component';
import { UserMetaComponent } from './user-meta/user-meta.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { CalendarComponent } from './calendar/calendar.component';
import { LoanComponent } from './loan/loan.component';



@NgModule({
  declarations: [UsersComponent, UserMetaComponent, UserEditComponent, CalendarComponent, LoanComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    UsersComponent
  ]
})
export class ClientsModule { }
