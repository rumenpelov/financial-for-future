import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Loan } from '../../common/Loan';

@Component({
  selector: 'app-loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.css']
})
export class LoanComponent implements OnInit {

  public loanForm: FormGroup ;

  @Output() cancel = new EventEmitter<boolean>();
  @Output() loan = new EventEmitter<Loan>();

  constructor(private readonly fb: FormBuilder) { }

  ngOnInit(): void {
    this.loanForm =  this.fb.group({
      amount: ['', Validators.compose([Validators.required, Validators.max(10000), Validators.pattern('[0-9]*')])],
      startDate: ['', Validators.compose([Validators.required])],
      endDate: ['',  Validators.compose([Validators.required])]
    });
  }

  get amount() { return this.loanForm.get('amount'); }

  updateLoanStartDate(date: string) {
    this.loanForm.patchValue({
      startDate: date
    });
  }

  updateLoanEndDate(date: string) {
    this.loanForm.patchValue({
      endDate: date
    });
  }

  saveLoan() {
    let [year, month, day] = this.loanForm.value.startDate.split('.');
    const d1 = new Date(year, +month - 1, day);
    [year, month, day] = this.loanForm.value.endDate.split('.');
    const d2 = new Date(year, +month - 1, day);
    if (d1.getTime() >= d2.getTime() ) {
      return;
    }

    const newLoan = {
      amount: this.loanForm.value.amount,
      startDate: this.loanForm.value.startDate,
      endDate: this.loanForm.value.endDate
    };

    this.loan.emit(newLoan);
  }

  onCancel() {
    this.cancel.emit(false);
  }
}
