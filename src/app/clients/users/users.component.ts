import { Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Client } from '../../common/Client';
import { Loan } from '../../common/Loan';
import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnDestroy {
  private clientsSubscription: Subscription;

  public clients: Client[];
  public selectedClient: Client;
  public refresh: boolean;
  public showLoanDialog: boolean;

  @ViewChild('cancel') cancel;
  @ViewChild('loan') loan;
  @HostListener('click', ['$event'])
  colapseLoanDialog(e: Event) {
    if (this.showLoanDialog) {
      this.loan.nativeElement.click();
    }
  }

  constructor(
    private readonly clientService: ClientService,
  ) { }

  ngOnInit(): void {
    this.clientsSubscription = this.clientService.clients$
      .subscribe( clients => {
        this.clients = clients;
      });
  }

  selectClient(client: Client): void {
    this.selectedClient = client;
  }

  deleteClient() {
    const updatedClients = this.clients.filter((client) => client.id !== this.selectedClient.id);
    this.clientService.updateClients(updatedClients);
    this.selectedClient = null;
  }

  updateClient(editedClient: Client) {
    const updatedClients = this.clients.map(client => {
      return client.id === editedClient.id ? editedClient : client;
    });
    this.clientService.updateClients(updatedClients);
    this.selectedClient = editedClient;
    this.cancel.nativeElement.click();
  }


  saveLoan(loan: Loan) {
    const updatedClients = this.clients.map(client => {
      if (client.id === this.selectedClient.id) {
        client.loans = [loan];
        return client;
      }
      return client;
    });

    this.clientService.updateClients(updatedClients);
    this.showLoanDialog = false;
}

  toggleRefresh() {
    this.refresh = true;
    setTimeout(() => {this.refresh = false; }, 0);
  }

  toggleShowLoanDialog() {
    this.showLoanDialog = !this.showLoanDialog;
  }

  eventStop(event: Event) {
    event.stopPropagation();
  }

  ngOnDestroy() {
    this.clientsSubscription.unsubscribe();
  }
}
