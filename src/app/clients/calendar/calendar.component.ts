import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {
  public date: Date;
  public months = ['January', 'February', 'March', 'April', 'May', 'June',
                  'July', 'August', 'September', 'October', 'November', 'December'];
  public week = ['Sun', 'Mon', 'Tue', 'Wed' , 'Thu', 'Fri', 'Sat'];
  public year: number;
  public month: number;
  public daySelected: number;
  public daysArr: number[];
  public selectedDate: string;
  public showDataPicker: boolean;

  @Input() label: string;
  @Output() update = new EventEmitter<string>();

  @HostListener('click', ['$event'])
  closeDataPicker(e: Event) {
    if (this.showDataPicker) {
      this.showDataPicker = false;
      this.update.emit(this.selectedDate);
    }
  }

  constructor() { }

  ngOnInit(): void {
    this.date = new Date();
    this.year  = this.date.getFullYear();
    this.month = this.date.getMonth();
    this.daySelected = this.date.getDay();

    this.updateData();
  }

  selectYear(e: Event, changeType: string) {
    e.stopPropagation();
    this.year = changeType === 'Inc' ? this.year + 1 : this.year - 1;
    this.date = new Date(this.year, this.month, this.daySelected);
    this.updateData();

    this.uppdateResult();
  }

  selectMonth(e: Event, changeType: string) {
    e.stopPropagation();
    if (changeType === 'Inc') {
      if (this.month + 1 < 12) {
        this.month++;
      } else {
        this.month = 0;
        this.year++;
      }
    } else if (changeType === 'Dec') {
      if (this.month - 1 >= 0) {
        this.month--;
      } else {
        this.month = 11;
        this.year--;
      }
    }

    this.date = new Date(this.year, this.month, this.daySelected);
    this.updateData();

    this.uppdateResult();
  }

  selectDay(e: Event, day: number) {
    e.stopPropagation();
    if (!day) {
      return;
    }
    this.daySelected = day;
    this.date = new Date(this.year, this.month, this.daySelected);

    this.uppdateResult();

    if (this.showDataPicker) {
      this.showDataPicker = false;
      this.update.emit(this.selectedDate);
    }
  }

  onShowDataPicker(e: Event) {
    e.stopPropagation();
    this.showDataPicker = true;
  }

  eventStop(event: Event) {
    event.stopPropagation();
  }

  private updateData() {
    const day = this.date.getDay();
    const dayArr = new Array(day).fill('');

    const lastDay = new Date(this.year, this.month + 1, 0);
    const days = lastDay.getDate();

    const daysArr = (new Array(days).fill(1)).reduce((acc, val, i) => [...acc, i + 1], []);
    this.daysArr = [...dayArr, ...daysArr];
  }

  private uppdateResult() {
    const formatVal = (str: string) => str.length < 2 ? 0 + str : str;
    this.selectedDate = `${ this.year }.${ formatVal(this.month + 1 + '') }.${ formatVal(this.daySelected + '') }`;
  }
}
