import { Component, Input, OnInit } from '@angular/core';
import { Client } from '../../common/Client';

@Component({
  selector: 'app-user-meta',
  templateUrl: './user-meta.component.html',
  styleUrls: ['./user-meta.component.css']
})
export class UserMetaComponent implements OnInit {
  public showPrivateInfo: boolean;

  @Input() client: Client;

  constructor() { }

  ngOnInit(): void {
  }

  togglePrivateInfo(): void {
    this.showPrivateInfo = !this.showPrivateInfo;
  }

}
