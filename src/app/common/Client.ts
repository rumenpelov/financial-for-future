import { Loan } from './Loan';

export class Client {
    id: number;
    firstName: string;
    middleName: string;
    lastName: string;
    address: {
        city: string;
        street: string;
        app: string;
    };
    startDate: string;
    balance: number;
    cards: string[];
    loans: Loan[];
}
