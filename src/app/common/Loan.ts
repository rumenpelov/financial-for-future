export class Loan {
    amount: number;
    startDate: string;
    endDate: string;
}
