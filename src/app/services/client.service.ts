import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Client } from '../common/Client.js';
import {clients} from '../dumy-data/clients-data.js';
import { StorageService } from './storage.service.js';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private readonly clientsSubject$ = new BehaviorSubject<Client[]>(this.retrieveClients());

  constructor(
    private readonly storage: StorageService,
  ) { }

  public get clients$(): Observable<any> {
    return this.clientsSubject$.asObservable();
  }

  public initializeClients() {
    this.storage.save('clients', JSON.stringify(clients));
    this.clientsSubject$.next(clients);
  }

  public updateClients(updatedClients) {
    this.storage.save('clients',  JSON.stringify(updatedClients));
    this.clientsSubject$.next(updatedClients);
  }

  public clearClients() {
    this.storage.delete('clients');
  }

  private retrieveClients() {
    return JSON.parse(this.storage.read('clients'));
  }

}
